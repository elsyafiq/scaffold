# iCollect Microservice Scaffold
## Description
This project is a scaffold for iCollect Micro Services. To have a standard
architecture, this scaffold is used as a starting point to develop micro 
services that will serve iCollect Front End Application.

## Components
This scaffold consist of 2 main directories which are `app` and `env`
- `app` directory contains application specific source code. Inside `app`
contains minimum `tests`, `src`, `http`, `cli`.
- `app/tests` contains all application test cases
- `app/src` contains application business specific codes. Also known as 
`Business Layer`.
- `app/http` contains RESTful API application code which resides in
`Application Layer`.
- `app/cli` contains command-line interface application which also resides in
`Application Layer`.
- `env` contains environment image docker file. Provided in this scaffold by
default is `workspace` dockerfile to help standardize development environment 
across teams.

## Installation
### Dependencies
Before creating project based on this scaffold, there are a few needed 
dependencies required:
- Docker
- Docker Compose
- Composer

To install docker, please visit this link: https://docs.docker.com/install/
and follow the instruction based on your OS.

There's no need to install docker compose for mac and windows as by default it
is already integrated in docker. But, for linux based system, please follow
this link: https://docs.docker.com/compose/install/#install-compose and select
linux in the given tab.

To install composer, refer to this link https://getcomposer.org/download/ and
follow the instruction there.

### Installation Steps
To use this scaffold as starting point for your micro service, run following
command in your terminal
```
php composer.phar create-project icollect/scaffold [path_name] dev-develop
```
Replace `[path_name]` with the location where you want to set up the scaffold

For unix based system, this scaffold include a Makefile to ease up the steps
to take to build `containers`. For windows, use listed commands to build
and run containers.
```
# build and spin up containers
PID=1000 GID=1000 docker-compose -p [service_name] -f environment/development.yml up --build -d
# Replace [service_name] with name of your service eg: import_service
```
```
# terminate and remove containers
PID=1000 GID=1000 docker-compose -p [service_name] -f environment/development.yml down
# Replace [service_name] with name of your service eg: import_service
```
```
# to login into workspace container
docker exec -it -u 1000 workspace bash
```
```
# to run all tests from inside workspace containers
docker exec -t -u 1000 workspace-debtor bash -c "cd /workspace && composer run tests"
```
```
# to run style checkers form inside workspace containers
docker exec -t -u 1000 workspace-debtor bash -c "cd /workspace && composer run phpcs"
```

For system except for windows, Makefile can be utilized to achieve above
commands. But before running those command, edit `Makefile` and replace 
`[service_name]` with your service e.d: `import_service`
```
# build and spin up containers
make up
```
```
# terminate and remove containers
make down
```
```
# to login into workspace container
make tty
```
```
# to run all tests from inside workspace containers
make tests
```
```
# to run style checkers form inside workspace containers
make check-style
```

## Issues, Bugs and Questions
For any issues, bugs or any questions regarding on how to utilize this
scaffold. Please send your query to 
`Syafiq Sarif <elsyafiq@collectius.com.my>`.

## Integration with other Framework
To integrate with other framework such as `Lumen` or `Laravel`. Install
`Lumen` or `Laravel` inside the `app` directory. To use `src` components
inside `Lumen` or `Laravel`, copy the item in composer.json under
`autoload.psr-4` and paste it in `lumen` or `laravel` composer. run command
`composer du` to autoload components in `src` to make it usable in `lumen`
or `laravel` projects.
