up:
	PID=$$(id -u) GID=$$(id -u) docker-compose -p [service_name] -f environment/development.yml up --build -d
down:
	PID=$$(id -u) GID=$$(id -u) docker-compose -p [service_name] -f environment/development.yml down
tty:
	docker exec -it -u $$(id -u) workspace bash
tests:
	docker exec -t -u $$(id -u) workspace bash -c "cd /workspace && composer run tests"
check-style:
	docker exec -t -u $$(id -u) workspace bash -c "cd /workspace && composer run phpcs"
